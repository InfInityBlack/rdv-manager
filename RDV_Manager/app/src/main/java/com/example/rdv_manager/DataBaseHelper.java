package com.example.rdv_manager;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private SQLiteDatabase database;

    //Table name
    public static final String TABLE_NAME = "RDV";

    //Table columns
    public static final String _ID = "_id";
    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String CONTACT = "contact";
    public static final String ADDRESS = "address";
    public static final String PHONE_NUMBER = "phone_number";

    //Database information
    public static final String DB_NAME = "RDV_MANAGER.DB";

    //database version
    public static final int DB_VERSION = 1;

    //creating table query
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY, " + TITLE + " TEXT NOT NULL, " + DATE + " TEXT NOT NULL, " +
            TIME + " TEXT NOT NULL, " + CONTACT + " TEXT, " + ADDRESS + " TEXT, " + PHONE_NUMBER + " TEXT);";

    public DataBaseHelper(Context context){
        super(context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " +  TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void open() throws SQLException{
        database = this.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public void add(RDV rdv){
        database.execSQL("INSERT INTO " + TABLE_NAME +
                " VALUES ("+ rdv.getId() +",'"+ rdv.getTitle() + "','" + rdv.getDate() + "','" + rdv.getTime() + "','" + rdv.getContact() + "','" +
                rdv.getAddress() + "','" + rdv.getPhone_number() + "');");
    }

    public void update(RDV rdv){
        database.execSQL("UPDATE " + TABLE_NAME + " SET " + TITLE +" = '" + rdv.getTitle() + "'," +  DATE + " = '" + rdv.getDate() + "'," +
                 TIME + " = '" + rdv.getTime() + "', " + CONTACT +" = '" + rdv.getContact() + "', " + ADDRESS + "= '" + rdv.getAddress() + "'," +
                PHONE_NUMBER + "= '" + rdv.getPhone_number() + "' WHERE " + _ID +" = " + rdv.getId() + ";");
    }

    public void delete(int id){
        database.execSQL("DELETE FROM " + TABLE_NAME + " WHERE " + _ID + " = " + id + ";");
    }

    public int getMaxID(){
        Cursor cursor = database.rawQuery("SELECT MAX(" + _ID + ") FROM " + TABLE_NAME + ";",null);
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public Cursor getAllRDV(){
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME,null);
        return cursor;
    }

}
