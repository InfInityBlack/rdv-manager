package com.example.rdv_manager;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private List<RDV> rdvList;
    private ActivityResultLauncher<Intent> createRDVResultLauncher;
    private ActivityResultLauncher<Intent> changedConfigResultLauncher;
    private RDVAdapter recyclerAdapter;
    private DataBaseHelper db;
    private AlarmManager alarmManager;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button createButton = findViewById(R.id.add_rdv_button);
        createButton.setOnClickListener(v -> openRDVCreationActivity());
        Button menuButton = findViewById(R.id.open_menu_button);
        menuButton.setOnClickListener(v -> openMenuActivity());

        db = new DataBaseHelper(this);
        db.open();
        rdvList = new ArrayList<>();
        Cursor savedRDV = db.getAllRDV();
        RDV c;
        boolean notFinished = savedRDV.moveToFirst();
        while(notFinished){
            int id = savedRDV.getInt(0);
            String title = savedRDV.getString(1);
            String date = savedRDV.getString(2);
            String time = savedRDV.getString(3);
            String contact = savedRDV.getString(4);
            String address = savedRDV.getString(5);
            String phone_number = savedRDV.getString(6);
            c = new RDV(id,title,date,time,contact,address,phone_number);
            rdvList.add(c);
            notFinished = savedRDV.moveToNext();
        }

        RDV.setStartingID(db.getMaxID()+1);

        setView(this.getResources().getConfiguration());

        createNotificationChannel();

        preferences = this.getPreferences(Context.MODE_PRIVATE);
        if(!preferences.contains("delay"))
            setDelay(NotifConfig.DAY.getDelay());

        createRDVResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        RDV newRDV = data.getParcelableExtra("rdv");
                        int oldRDV = findRDVbyID(newRDV.getId());
                        if(oldRDV == -1){
                            rdvList.add(newRDV);
                            recyclerAdapter.notifyItemInserted(rdvList.size()-1);
                            db.add(newRDV);
                            setAlarm(newRDV.getDate() + " " + newRDV.getTime(),newRDV.getTitle(),newRDV.getContact(),newRDV.getId());
                        }
                        else{
                            if(newRDV.isReadyForDeletion()){
                                rdvList.remove(oldRDV);
                                recyclerAdapter.notifyItemRemoved(oldRDV);
                                cancelAlarm(oldRDV);
                                db.delete(newRDV.getId());
                            }
                            else{
                                rdvList.set(oldRDV,newRDV);
                                recyclerAdapter.notifyItemChanged(oldRDV);
                                cancelAlarm(oldRDV);
                                setAlarm(newRDV.getDate() + " " + newRDV.getTime(),newRDV.getTitle(),newRDV.getContact(),newRDV.getId());
                                db.update(newRDV);
                            }
                        }
                    }
                });
        changedConfigResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if(result.getResultCode() == Activity.RESULT_OK){
                        Intent data = result.getData();
                        NotifConfig config = (NotifConfig) data.getSerializableExtra("time_setting");
                        setDelay(config.getDelay());
                    }
                }
        );
    }

    public void setDelay(long delay){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("delay",delay);
        editor.apply();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        Toast.makeText(this,"landscape engaged",Toast.LENGTH_LONG).show();
        setView(newConfig);


    }

    public void setAlarm(@NonNull String time,String title, String contact,int id){
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notifyIntent = new Intent(this, RDVReminderReceiver.class);
        notifyIntent.putExtra("title",title);
        notifyIntent.putExtra("contact",contact);
        notifyIntent.putExtra("delay",preferences.getLong("delay",0));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,id,notifyIntent,0);
        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.getDefault()).parse(time);
            long delay = preferences.getLong("delay",0);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,date.getTime()-delay,AlarmManager.INTERVAL_HALF_DAY,pendingIntent);
        } catch (ParseException e) {
            Toast.makeText(this,"An error has occurred while setting the alarm",Toast.LENGTH_LONG).show();
        }
    }

    public void cancelAlarm(int id){
        Intent notifyIntent = new Intent(this, RDVReminderReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,id,notifyIntent,0);
        if(alarmManager == null){
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        }
        alarmManager.cancel(pendingIntent);
        Toast.makeText(this,"alarm cancelled",Toast.LENGTH_LONG).show();
    }

    public void setView(Configuration config){
        if(config.orientation == Configuration.ORIENTATION_LANDSCAPE){
            RecyclerView maVueRecycle = findViewById(R.id.main_liste);
            maVueRecycle.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            maVueRecycle.setLayoutManager(llm);
            recyclerAdapter = new RDVAdapter(rdvList, this::openRDVDetailActivity, true);
            maVueRecycle.setAdapter(recyclerAdapter);
        }
        if(config.orientation == Configuration.ORIENTATION_PORTRAIT){
            RecyclerView maVueRecycle = findViewById(R.id.main_liste);
            maVueRecycle.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            maVueRecycle.setLayoutManager(llm);
            recyclerAdapter = new RDVAdapter(rdvList, this::openRDVDetailActivity, false);
            maVueRecycle.setAdapter(recyclerAdapter);
        }
    }

    public void openRDVDetailActivity(RDV selected){
        Intent intent = new Intent(this, RDVDetailActivity.class);
        intent.putExtra("rdv",selected);
        createRDVResultLauncher.launch(intent);
    }

    public void openRDVCreationActivity(){
        Intent intent = new Intent(this,RDVCreationActivity.class);
        createRDVResultLauncher.launch(intent);
    }

    public void openMenuActivity(){
        Intent intent = new Intent(this,MenuActivity.class);
        changedConfigResultLauncher.launch(intent);
    }

    public int findRDVbyID(int id){
        for(int i = 0; i< rdvList.size(); i++){
            if(rdvList.get(i).getId() == id)
                return i;
        }
        return -1;
    }

    public void createNotificationChannel(){
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
            CharSequence name = "rdvreminderchannel";
            String description = "channel for alarm manager";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("rdvreminder",name,importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
