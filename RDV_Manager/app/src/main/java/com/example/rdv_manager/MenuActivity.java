package com.example.rdv_manager;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioGroup;

public class MenuActivity extends AppCompatActivity {
    private RadioGroup group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button confirmButton = findViewById(R.id.menu_confirm_button);
        confirmButton.setOnClickListener(v -> confirmSettings());

        group = findViewById(R.id.menu_radio_group);
        //group.setOnCheckedChangeListener((radioGroup, i) -> buttonClicked());

    }

    public void buttonClicked(){

    }

    public void confirmSettings(){
        NotifConfig config;
        int checkedId = group.getCheckedRadioButtonId();
        switch(checkedId){
            case R.id.menu_radio_week:
                config = NotifConfig.WEEK;
                break;
            case R.id.menu_radio_2day:
                config = NotifConfig.TWODAYS;
                break;
            default:
                config = NotifConfig.DAY;
                break;
        }
        Intent intent = new Intent();
        intent.putExtra("time_setting", config);
        setResult(RESULT_OK, intent);
        finish();
    }
}