package com.example.rdv_manager;

import android.app.AlarmManager;

public enum NotifConfig {
    WEEK(AlarmManager.INTERVAL_DAY*7),DAY(AlarmManager.INTERVAL_DAY),TWODAYS(AlarmManager.INTERVAL_DAY*2);
    private final long delay;

    NotifConfig(long delay){
        this.delay = delay;
    }

    public long getDelay(){
        return delay;
    }
}
