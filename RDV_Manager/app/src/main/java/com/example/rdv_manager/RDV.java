package com.example.rdv_manager;

import android.os.Parcel;
import android.os.Parcelable;

public class RDV implements Parcelable {
    private static int id = 0;
    private int rdvId;
    private String title,date,time,contact,address,phone_number;
    private boolean state;

    public RDV(String title,String date,String time,String contact,String address, String phone_number){
        rdvId = id++;
        this.title = title;
        this.time = time;
        this.date = date;
        this.contact = contact;
        this.address = address;
        this.phone_number = phone_number;
        state = false;
    }

    public RDV(Parcel in) {
        rdvId = in.readInt();
        title = in.readString();
        date = in.readString();
        time = in.readString();
        contact = in.readString();
        address = in.readString();
        phone_number = in.readString();
        state = in.readByte() != 0;
    }

    public RDV(int id,String title,String date,String time,String contact,String address, String phone_number){
        rdvId = id;
        this.title = title;
        this.time = time;
        this.date = date;
        this.contact = contact;
        this.address = address;
        this.phone_number = phone_number;
        state = false;
    }

    public static void setStartingID(int val){
        id = val;
    }

    public static final Creator<RDV> CREATOR = new Creator<RDV>() {
        @Override
        public RDV createFromParcel(Parcel in) {
            return new RDV(in);
        }

        @Override
        public RDV[] newArray(int size) {
            return new RDV[size];
        }
    };

    public int getId(){ return this.rdvId; }

    public void setPhone_number(String phone_number) { this.phone_number = phone_number; }

    public void setAddress(String address) { this.address = address; }

    public void setContact(String contact) { this.contact = contact; }

    public void setTime(String time) { this.time = time; }

    public void setDate(String date) { this.date = date; }

    public void setTitle(String title) {this.title = title; }

    public void setPhoneNumber(String phone_number){ this.phone_number = phone_number;}

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getTime(){return time;}

    public String getAddress() {
        return address;
    }

    public String getPhone_number() {return phone_number;}

    public String getContact() { return contact; }

    public boolean getState(){ return state; }

    public void setState(boolean newState){
        this.state = newState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(getId());
        parcel.writeString(getTitle());
        parcel.writeString(getDate());
        parcel.writeString(getTime());
        parcel.writeString(getContact());
        parcel.writeString(getAddress());
        parcel.writeString(getPhone_number());
        parcel.writeByte((byte) (getState()?0:1));
    }

    public boolean isReadyForDeletion() {
        return title == null && date == null && time == null && contact == null;
    }
}
