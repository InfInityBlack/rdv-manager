package com.example.rdv_manager;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.Calendar;

public abstract class RDVActivityAbstract extends AppCompatActivity {
    private DatePickerDialog.OnDateSetListener onDate;
    private TimePickerDialog.OnTimeSetListener onTime;
    private ActivityResultLauncher<Void> contactRDVResultLauncher;
    private final int READ_CONTACTS_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contactRDVResultLauncher = registerForActivityResult(new ActivityResultContracts.PickContact(),
                uri -> {
                    if(uri != null){
                        Cursor c =  getContentResolver().query(uri, null, null, null, null);
                        if(c.moveToFirst()){
                            String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                            int index_hasPhone = c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                            String hasPhone = c.getString(index_hasPhone);
                            int index_name = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                            String name = c.getString(index_name);
                            setChosenContact(name);
                            if (hasPhone.equalsIgnoreCase("1"))
                            {
                                Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                                phones.moveToFirst();
                                int index_number = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                                String number = phones.getString(index_number);
                                setChosenNumber(number);
                            }
                            c.close();
                        }
                    }
                });
    }

    public void showDatePicker(){
        DatePickerFragment date = new DatePickerFragment();
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Bundle args = new Bundle();
        args.putInt("year",year);
        args.putInt("month",month);
        args.putInt("day",day);

        date.setArguments(args);
        date.setCallBack(onDate);
        date.show(getSupportFragmentManager(),"Date Picker");
    }

    public void setDateListener(DatePickerDialog.OnDateSetListener onDate){
        this.onDate = onDate;
    }

    public void setTimeListener(TimePickerDialog.OnTimeSetListener onTime){
        this.onTime = onTime;
    }

    public void showTimePicker(){
        TimePickerFragment time = new TimePickerFragment();
        Bundle args = new Bundle();
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minutes = c.get(Calendar.MINUTE);

        args.putInt("hour",hour);
        args.putInt("minutes",minutes);

        time.setArguments(args);
        time.setCallBack(onTime);
        time.show(getSupportFragmentManager(), "Time Picker");
    }

    public void cancel(){
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    public void selectContact(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED)
            contactRDVResultLauncher.launch(null);
        else
            requestContactPermission();
    }

    public void requestContactPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_CONTACTS)){
            new AlertDialog.Builder(this)
                    .setTitle("Permission required")
                    .setMessage("Permission required to access contact list")
                    .setPositiveButton("ok", (dialogInterface, i) -> ActivityCompat.requestPermissions(RDVActivityAbstract.this,new String[] {Manifest.permission.READ_CONTACTS},READ_CONTACTS_PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialogInterface, i) -> dialogInterface.dismiss())
                    .create().show();
        }
        else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.READ_CONTACTS},READ_CONTACTS_PERMISSION_CODE);
        }
    }

    public abstract void setChosenContact(String contact);

    public abstract  void setChosenNumber(String number);
}
