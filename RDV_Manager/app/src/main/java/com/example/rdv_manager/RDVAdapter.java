package com.example.rdv_manager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RDVAdapter extends RecyclerView.Adapter<RDVAdapter.ViewHolder> {
    private final List<RDV> appointments;
    private final OnItemClickListener listener;
    private boolean isLandscape;

    public RDVAdapter(List<RDV> list, OnItemClickListener listener, boolean isLandscape){
        this.appointments = list;
        this.listener = listener;
        this.isLandscape = isLandscape;
    }

    @NonNull
    @Override
    public RDVAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(isLandscape)
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rdv_list_landscape,parent,false);
        else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rdv_list_portrait,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(appointments.get(position),listener);
        RDV rdv = appointments.get(position);
        holder.getTextTitle().setText(rdv.getTitle());
        holder.getTextDate().setText(rdv.getDate());
        if(isLandscape){
            holder.getTextTime().setText(rdv.getTime());
            holder.getTextContact().setText(rdv.getContact());
            holder.getTextAddress().setText(rdv.getAddress());
            holder.getTextPhone().setText(rdv.getPhone_number());
        }
    }

    @Override
    public int getItemCount() {
        return appointments.size();
    }

    public interface OnItemClickListener {
        void onItemClick(RDV moment);
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView title;
        private final TextView date;
        private final TextView time;
        private final TextView contact;
        private final TextView address;
        private final TextView phone_number;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.list_title);
            date = itemView.findViewById(R.id.list_date);
            time = itemView.findViewById(R.id.list_time);
            contact = itemView.findViewById(R.id.list_contact);
            address = itemView.findViewById(R.id.list_address);
            phone_number = itemView.findViewById(R.id.list_phone);
        }

        public void bind(RDV rdv, OnItemClickListener listener){
            itemView.setOnClickListener(v -> listener.onItemClick(rdv));
        }

        public TextView getTextTitle(){
            return title;
        }

        public TextView getTextDate(){
            return date;
        }

        public TextView getTextTime(){
            return time;
        }

        public TextView getTextContact(){
            return contact;
        }

        public TextView getTextAddress(){
            return address;
        }

        public TextView getTextPhone(){
            return phone_number;
        }
    }
}
