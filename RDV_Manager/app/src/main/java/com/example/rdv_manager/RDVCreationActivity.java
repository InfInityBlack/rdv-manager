package com.example.rdv_manager;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class RDVCreationActivity extends RDVActivityAbstract {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rdvcreation);

        setDateListener((datePicker, selectedYear, selectedMonth, selectedDay) -> {
            TextView textDate = findViewById(R.id.create_date_text);
            String date = "";
            selectedMonth++;
            if(selectedDay < 10)
                date += "0"+selectedDay+"/";
            else
                date +=selectedDay+"/";
            if(selectedMonth<10)
                date +="0"+selectedMonth+"/";
            else
                date+=selectedMonth+"/";
            date+=selectedYear;
            textDate.setText(date);
        });

        setTimeListener((timePicker, selectedHour, selectedMinute) ->{
            TextView textTime = findViewById(R.id.create_time_text);
            String time = "";
            if(selectedHour<10)
                time+="0"+selectedHour+":";
            else
                time+=selectedHour+":";
            if(selectedMinute <10)
                time+="0"+selectedMinute;
            else
                time+=selectedMinute;
            textTime.setText(time);
        });

        TextView textDate = findViewById(R.id.create_date_text);
        textDate.setOnClickListener(v -> showDatePicker());

        TextView textTime = findViewById(R.id.create_time_text);
        textTime.setOnClickListener(v -> showTimePicker());

        Button createButton = findViewById(R.id.create_create_button);
        createButton.setOnClickListener(v -> createRDV());

        Button cancelButton = findViewById(R.id.create_cancel_button);
        cancelButton.setOnClickListener(v -> cancel());

        Button contactListButton = findViewById(R.id.create_contact_button);
        contactListButton.setOnClickListener(v -> selectContact());
    }

    @Override
    public void setChosenContact(String contact) {
        ((TextView)findViewById(R.id.create_contact_text)).setText(contact);
    }

    @Override
    public void setChosenNumber(String number) {
        ((TextView)findViewById(R.id.create_phone_text)).setText(number);
    }

    public void createRDV(){
        String title = ((EditText)findViewById(R.id.create_title_text)).getText().toString();
        String date = ((TextView)findViewById(R.id.create_date_text)).getText().toString();
        String time = ((TextView)findViewById(R.id.create_time_text)).getText().toString();
        String contact = ((EditText)findViewById(R.id.create_contact_text)).getText().toString();
        String address = ((EditText)findViewById(R.id.create_address_text)).getText().toString();
        String phone_number = ((EditText)findViewById(R.id.create_phone_text)).getText().toString();
        RDV rdv = new RDV(title,date,time,contact,address,phone_number);
        Intent intent = new Intent();
        intent.putExtra("rdv", rdv);
        setResult(RESULT_OK, intent);
        finish();
    }
}