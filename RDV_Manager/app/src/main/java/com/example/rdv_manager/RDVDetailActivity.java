package com.example.rdv_manager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RDVDetailActivity extends RDVActivityAbstract {
    private RDV shownRDV;
    private final int CALL_PHONE_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rdvdetail);
        Intent intent = getIntent();
        shownRDV = intent.getParcelableExtra("rdv");
        ((EditText)findViewById(R.id.detail_title_text)).setText(shownRDV.getTitle());
        ((TextView)findViewById(R.id.detail_date_text)).setText(shownRDV.getDate());
        ((TextView)findViewById(R.id.detail_time_text)).setText(shownRDV.getTime());
        ((EditText)findViewById(R.id.detail_contact_text)).setText(shownRDV.getContact());
        ((EditText)findViewById(R.id.detail_phone_text)).setText(shownRDV.getPhone_number());
        ((EditText)findViewById(R.id.detail_address_text)).setText(shownRDV.getAddress());

        setDateListener((datePicker, selectedYear, selectedMonth, selectedDay) -> {
            TextView textDate = findViewById(R.id.detail_date_text);
            String date = "";
            selectedMonth++;
            if(selectedDay < 10)
                date += "0"+selectedDay+"/";
            else
                date +=selectedDay+"/";
            if(selectedMonth<10)
                date +="0"+selectedMonth+"/";
            else
                date+=selectedMonth+"/";
            date+=selectedYear;
            textDate.setText(date);
        });

        setTimeListener((timePicker, selectedHour, selectedMinute) ->{
            TextView textTime = findViewById(R.id.detail_time_text);
            String time = "";
            if(selectedHour<10)
                time+="0"+selectedHour+":";
            else
                time+=selectedHour+":";
            if(selectedMinute <10)
                time+="0"+selectedMinute;
            else
                time+=selectedMinute;
            textTime.setText(time);
        });

        TextView textDate = findViewById(R.id.detail_date_text);
        textDate.setOnClickListener(v -> showDatePicker());

        TextView textTime = findViewById(R.id.detail_time_text);
        textTime.setOnClickListener(v -> showTimePicker());

        Button createButton = findViewById(R.id.detail_edit_button);
        createButton.setOnClickListener(v -> editRDV());

        Button cancelButton = findViewById(R.id.detail_cancel_button);
        cancelButton.setOnClickListener(v -> cancel());

        Button deleteButton = findViewById(R.id.detail_delete_button);
        deleteButton.setOnClickListener(v -> deleteRDV());

        Button mapButton = findViewById(R.id.detail_map_button);
        mapButton.setOnClickListener(v -> launchMaps());

        Button callButton = findViewById(R.id.detail_call_button);
        callButton.setOnClickListener(v -> callNumber());

        Button contactListButton = findViewById(R.id.detail_contact_button);
        contactListButton.setOnClickListener(v -> selectContact());
    }

    public void editRDV(){
        shownRDV.setTitle(((EditText)findViewById(R.id.detail_title_text)).getText().toString());
        shownRDV.setDate(((TextView)findViewById(R.id.detail_date_text)).getText().toString());
        shownRDV.setTime(((TextView)findViewById(R.id.detail_time_text)).getText().toString());
        shownRDV.setContact(((EditText)findViewById(R.id.detail_contact_text)).getText().toString());
        shownRDV.setAddress(((EditText)findViewById(R.id.detail_address_text)).getText().toString());
        shownRDV.setPhoneNumber(((EditText)findViewById(R.id.detail_phone_text)).getText().toString());
        Intent intent = new Intent();
        intent.putExtra("rdv", shownRDV);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void deleteRDV(){
        shownRDV.setTitle(null);
        shownRDV.setDate(null);
        shownRDV.setTime(null);
        shownRDV.setContact(null);
        shownRDV.setAddress(null);
        shownRDV.setPhoneNumber(null);
        Intent intent = new Intent();
        intent.putExtra("rdv", shownRDV);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void launchMaps() {
        String map = "http://maps.google.co.in/maps?q=" + shownRDV.getAddress();
        Uri gmmIntentUri = Uri.parse(map);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        startActivity(mapIntent);
    }

    private void callNumber(){
        String number = shownRDV.getPhone_number();
        if(number == null || number.equals("")){
            Toast.makeText(this,"phone number not set",Toast.LENGTH_SHORT).show();
            return;
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+number));
            startActivity(callIntent);
        }
        else{
            requestCallPermission();
        }

    }

    public void requestCallPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CALL_PHONE)){
            new AlertDialog.Builder(this)
                    .setTitle("Permission required")
                    .setMessage("Permission required to make a call")
                    .setPositiveButton("ok", (dialogInterface, i) -> ActivityCompat.requestPermissions(RDVDetailActivity.this,new String[] {Manifest.permission.CALL_PHONE},CALL_PHONE_PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialogInterface, i) -> dialogInterface.dismiss())
                    .create().show();
        }
        else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CALL_PHONE},CALL_PHONE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,@NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == CALL_PHONE_PERMISSION_CODE){
            if(grantResults.length>0 && grantResults[0] ==PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this,"permission to make calls granted",Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this,"permission to make calls refused",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void setChosenContact(String contact) {
        ((TextView)findViewById(R.id.detail_contact_text)).setText(contact);
    }

    @Override
    public void setChosenNumber(String number) {
        ((TextView)findViewById(R.id.detail_phone_text)).setText(number);
    }
}